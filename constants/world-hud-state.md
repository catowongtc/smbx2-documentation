# World Hud State constants

Constants for the world map hud state.

| Constant | Value | Description |
| --- | --- | --- |
| WHUD_ALL | 0 | All HUD elements are visible. |
| WHUD_ONLY_OVERLAY | 1 | Only the HUD overlay is visible, with text and icons hidden. |
| WHUD_NONE | 2 | All HUD elements are hidden. |