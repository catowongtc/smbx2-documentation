# Level (Overworld)

This page is for the Level tiles on the overworld. If you're looking for the Level class when accessed from within a level, go [here](reference/level-overworld.md).

## Static Members

These elements are static to the class itself and can be accessed like so:
```lua
function onStart()
    local numberOfLevels = Level.count()
    Misc.dialog(numberOfLevels)
end
```

### Static functions
{STARTTABLE}
   {NAME} Function
    {RET} Return Values
   {DESC} Description
====
   {NAME} Level.count()
    {RET} [number](types/number.md) amount
   {DESC} Returns the number of levels on the overworld.
====
   {NAME} Level.get()
    {RET} [table](types/table.md) of [Level](reference/level-overworld.md) levels
   {DESC} Returns a table of all levels on the overworld.
====
   {NAME} Level.get(

[number](types/number.md) or [table](types/table.md) of [number](types/number.md) ids

)
    {RET} [table](types/table.md) of [Level](reference/level-overworld.md) levels
   {DESC} Returns a table of all levels on the overworld with matching ID(s).
====
   {NAME} Level.findByName(

[string](types/string.md) searchText

)
    {RET} [table](types/table.md) of [Level](reference/level-overworld.md) levels
   {DESC} Returns all levels where the title field matches the given searchText.
====
   {NAME} Level.findByFilename(

[string](types/string.md) searchText

)
    {RET} [table](types/table.md) of [Level](reference/level-overworld.md) levels
   {DESC} Returns all levels where the filename field matches the given searchText.
====
   {NAME} Level.getByName(

[string](types/string.md) title or [nil](types/nil.md)

)
    {RET} [Level](reference/level-overworld.md) levels
   {DESC} Returns the level with the given title. If the level is not found, nil is returned.
====
   {NAME} Level.getByFilename(

[string](types/string.md) filename or [nil](types/nil.md)

)
    {RET} [Level](reference/level-overworld.md) levels
   {DESC} Returns the level with the given filename. If the level is not found, nil is returned.
{ENDTABLE}

## Instance Members

Instance members must be accessed through a reference to a specific [Level](/reference/level-overworld.md) object.
```lua
local myLevel = Level.getByName("test")
if myLevel then
    myLevel.visible = true
end
```
<Note type="warning">Attempting to call instance members statically will result in an error!</Note>

### Instance Fields

<!-- (Consider an icon for "read only".) -->
{STARTTABLE}
   {TYPE} Type
  {FIELD} Field
     {RO} Read-only?
   {DESC} Description
====
   {TYPE} [number](/types/number.md)
  {FIELD} x
     {RO} No
   {DESC} X-Coordinate
====
   {TYPE} [number](/types/number.md)
  {FIELD} y
     {RO} No
   {DESC} Y-Coordinate
====
   {TYPE} [number](/types/number.md)
  {FIELD} goToX
     {RO} No
   {DESC} X-Coordinate of the destination (if set to warp to a map coordinate)
====
   {TYPE} [number](/types/number.md)
  {FIELD} goToY
     {RO} No
   {DESC} Y-Coordinate of the destination (if set to warp to a map coordinate)
====
   {TYPE} [Win Type](/constants/level-victory.md)
  {FIELD} topExitType
     {RO} No
   {DESC} The exit type that triggers the top path.
====
   {TYPE} [Win Type](/constants/level-victory.md)
  {FIELD} leftExitType
     {RO} No
   {DESC} The exit type that triggers the left path.
====
   {TYPE} [Win Type](/constants/level-victory.md)
  {FIELD} bottomExitType
     {RO} No
   {DESC} The exit type that triggers the bottom path.
====
   {TYPE} [Win Type](/constants/level-victory.md)
  {FIELD} rightExitType
     {RO} No
   {DESC} The exit type that triggers the right path.
====
   {TYPE} [number](/types/number.md)
  {FIELD} levelWarpNumber
     {RO} No
   {DESC} The index of the warp at which to enter the level.
====
   {TYPE} [bool](/types/bool.md)
  {FIELD} isPathBackground
     {RO} No
   {DESC} Whether the level has a path background behind it.
====
   {TYPE} [bool](/types/bool.md)
  {FIELD} isBigBackground
     {RO} No
   {DESC} Whether the level has a big path background behind it.
====
   {TYPE} [bool](/types/bool.md)
  {FIELD} isGameStartPoint
     {RO} No
   {DESC} Whether the level is where the game begins.
====
   {TYPE} [bool](/types/bool.md)
  {FIELD} isAlwaysVisible
     {RO} No
   {DESC} Whether the level is always visible.
====
   {TYPE} [bool](/types/bool.md)
  {FIELD} visible
     {RO} No
   {DESC} Whether the level is currently visible.
====
   {TYPE} [string](/types/string.md)
  {FIELD} title
     {RO} Yes
   {DESC} The title that shows up in the world map HUD.
====
   {TYPE} [string](/types/string.md)
  {FIELD} filename
     {RO} Yes
   {DESC} The filename of the level file to load on enter.
{ENDTABLE}